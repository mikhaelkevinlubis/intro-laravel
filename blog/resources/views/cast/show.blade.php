@extends('layouts.master')

@section('judul')
    {{ $getCast->nama }}
@endsection

@section('content')
<div class="row">
    <div class="col-sm-3">

    </div>
    <div class="card col-sm-6">
        <h5 class="card-header">
            {{ $getCast->nama }}</h5>
        <div class="card-body">
        <img src="{{ asset('img/user.png') }}" height="300" width="220" alt="..." class="">
        <br>
          <h5 class="card-title">Age: {{ $getCast->umur }}</h5>
          <p class="card-text">Bio: {{ $getCast->bio }}</p>
          <div class="d-flex justify-content-end">
            <a href="/cast" class="btn btn-primary text-right">Back</a>
          </div>
        </div>
      </div>
    <div class="col-sm-3">

    </div>
</div>

@endsection