@extends('layouts.master')

@section('judul')
    Ubah Cast
@endsection

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Form Ubah Cast</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>

    <div class="card-body">
      <form action="/cast/{{ $cast->id }}" method="POST">
          @csrf
          @method('PUT')
          Cast Name :<br>
          <input type="text" name='nama' id="nama" value="{{ $cast->nama }}"><br><br>
          @error('nama')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          Age : <br> 
          <input type="number" min="0" name='umur' id="umur" value="{{ $cast->umur }}" ><br><br>
          @error('umur')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          Bio : <br>
          <textarea name="bio" id="bio" cols="30" rows="10">{{ $cast->bio }}</textarea><br><br>
          <button type="submit">Update</button>
          <button type="reset">Reset</button>
      </form>
  </div>
    <!-- /.card-body -->

  </div>

@endsection