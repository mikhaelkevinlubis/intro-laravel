@extends('layouts.master')

@section('judul')
    Tambah Cast
@endsection

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Form Tambah Cast</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>

    <div class="card-body">
        <form action="/cast" method="POST">
            @csrf
            Cast Name :<br>
            <input type="text" name='nama' id="nama"><br><br>
            @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            Age : <br> 
            <input type="number" name='umur' id="umur"><br><br>
            @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            Bio : <br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
            <button type="submit">Add</button>
            <button type="reset">Reset</button>
        </form>
    </div>
    <!-- /.card-body -->

  </div>

@endsection