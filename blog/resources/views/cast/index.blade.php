@extends('layouts.master')

@section('judul')
    List Cast Film
@endsection


@section('content')
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('delete'))
    <div class="alert alert-danger">
        {{ session('delete') }}
    </div>
@endif
<br>
<br>
    <div class="row">
        @forelse ($cast as $key=>$value)
        <div class="col-md-3 d-flex justify-content-center">
            <div class="card" style="width: 18rem;">
                <img src="{{ asset('img/user.png') }}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title text-center">{{ $value->nama }}({{ $value->umur }} Tahun)</h5>
                    <br>
                        <a href="/cast/{{$value->id}}" class="btn btn-info" >Show</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/cast/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                </div>
              </div>
        </div>
        
    @empty
        <p>No Posts</p>
    @endforelse
    </div>
@endsection