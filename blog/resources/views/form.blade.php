<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <form action="/welcome" method="POST"><h1>Buat Account Baru</h1>
        @csrf
        <h3>Sign Up Form</h3>
        First name :
        <input type="text" placeholder="Mikhael" name='firstName' required><br><br>
        Last name :
        <input type="text" placeholder="Kevin" name='lastName'><br><br>
        Gender : <br> <br>
        <input type="radio" name="gender" value="Male">Male <br>
        <input type="radio" name="gender" value="Female">Female <br><br>
        Nationality <br> <br>
        <select name="nationality">
           <option value="Indonesia">Indonesia</option>
           <option value="Frace">France</option>
           <option value="Germany">Germany</option>
        </select> <br><br>
        Language Spoken <br><br>
        <input type="checkbox" name="lang">Bahasa Indonesia <br>
        <input type="checkbox" name="lang">English  <br>
        <input type="checkbox" name="lang">Other    <br><br>
        Bio <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <button type="submit">Sign Up</button>
    </form>
    <a href="/ "><button>Back</button></a>
</body>
</html>